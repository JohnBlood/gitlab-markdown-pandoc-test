# I got this make file from: https://github.com/fawkesley/markdown-to-epub-mobi/blob/master/Makefile. Be sure to check out the original,

PARTS = $(wildcard markdown/*.md)

EPUB_FILE = epub/book.epub
PDF_FILE = pdf/book.pdf


.PHONY: all
all: $(EPUB_FILE) $(PDF_FILE)

.PHONY: clean
clean:
	rm -f build/*

.PHONY: epub
epub: $(EPUB_FILE)

.PHONY: pdf
pdf: $(PDF_FILE)

$(EPUB_FILE): clean $(PARTS) 
	pandoc \
		-o $(EPUB_FILE) \
		$(PARTS) \
		--table-of-contents

$(PDF_FILE): $(PARTS) meta/title.txt
	pandoc \
		-o $(PDF_FILE) \
		$(PARTS) \
		--toc