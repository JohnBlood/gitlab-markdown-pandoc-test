# gitlab-markdown-pandoc-test

This is an attempt to use Gitlab's CI to automatically convert Markdown to PDF and Epub.

This is inspired by this [blog post](https://medium.com/isovera/devops-for-presentations-reveal-js-markdown-pandoc-gitlab-ci-34d07d2c1011).